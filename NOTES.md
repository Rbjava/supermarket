# Notes

Please add here any notes, assumptions and design decisions that might help up understand your though process.

Notes:
1. Introduced ApacheCommons Collections jar, to use some helper methods.
2. I am assuming some simplicity for the discount, for example the buy one get one free.
Not comparing the actual prices which ones are higher or lower, etc.
3. For the weight discount, i am assuming the promo is to have half price for a full unit,
and original price applied to the non full unit.
Example: 1.5kg, the 1kg would have the promo of half price, the 0.5 kg will have full price
4. I modified the Basket class slightly to allow what kind of discounts to be applied.
5. Created several Discount classes implementing Discount interface.