package kata.supermarket;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the manager class which coordinates different discounts to be applied and invoke calculations
 */
class DiscountManagerTest {

    @Test
    void applyDiscountBuyOneGetOneFreeWhenThereAreTwoItemsInBasket() {
        Item itemOne = new Product(new BigDecimal("0.49")).oneOf();
        Item itemTwo = new Product(new BigDecimal("1.50")).oneOf();

        List<Item> amounts = new ArrayList<>();
        amounts.add(itemOne);
        amounts.add(itemTwo);

        DiscountManager discountManager = new DiscountManager();
        discountManager.applyDiscount(new DiscountBuyOneGetOneFree(amounts));

        Assert.assertEquals(new BigDecimal("0.49"), discountManager.getTotalDiscount());
    }

    @Test
    void removeDiscountBuyOneGetOneFreeWithTwoItems() {
        Item itemOne = new Product(new BigDecimal("0.49")).oneOf();
        Item itemTwo = new Product(new BigDecimal("1.50")).oneOf();

        List<Item> amounts = new ArrayList<>();
        amounts.add(itemOne);
        amounts.add(itemTwo);

        DiscountManager discountManager = new DiscountManager();
        DiscountBuyOneGetOneFree firstDiscount = new DiscountBuyOneGetOneFree(amounts);
        discountManager.applyDiscount(firstDiscount);

        Assert.assertEquals(new BigDecimal("0.49"), discountManager.getTotalDiscount());

        discountManager.removeDiscount(firstDiscount);
        Assert.assertEquals(new BigDecimal("0.00"), discountManager.getTotalDiscount());
    }

    @Test
    void applyDiscountBuyOneKiloAndTwoKilosInBasket() {
        WeighedProduct weighedProduct1 = new WeighedProduct(new BigDecimal("100"));
        Item item1 = weighedProduct1.weighing(new BigDecimal("1.0"));

        WeighedProduct weighedProduct2 = new WeighedProduct(new BigDecimal("50"));
        Item item2 = weighedProduct2.weighing(new BigDecimal("2.0"));

        List<Item> itemsToCalculate = Stream.of(item1, item2).collect(Collectors.toList());

        Assert.assertTrue(itemsToCalculate.contains(item1));
        Assert.assertTrue(itemsToCalculate.contains(item2));

        DiscountManager discountManager = new DiscountManager();
        discountManager.applyDiscount(new Discount1KiloHalfPrice(itemsToCalculate));

        Assert.assertEquals(new BigDecimal("100.00"), discountManager.getTotalDiscount());
    }

}