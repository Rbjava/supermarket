package kata.supermarket;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Test cases for the basket with different items and prices
 *
 */
public class BasketWithDiscountTest {
    @Test
    void basketWithOneDiscount() {
        Item itemOne = new Product(new BigDecimal("0.49")).oneOf();
        Item itemTwo = new Product(new BigDecimal("1.50")).oneOf();

        List<Item> amounts = new ArrayList<>();
        amounts.add(itemOne);
        amounts.add(itemTwo);

        Basket basket = new Basket();
        basket.add(itemOne);
        basket.add(itemTwo);

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(amounts);
        basket.addDiscount(discountBuyOneGetOneFree);

        Assert.assertEquals(new BigDecimal("1.50"), basket.total());

    }

    @Test
    void basketWithOneDiscountAppliedThenRemoved() {
        Item itemOne = new Product(new BigDecimal("0.49")).oneOf();
        Item itemTwo = new Product(new BigDecimal("1.50")).oneOf();

        List<Item> amounts = new ArrayList<>();
        amounts.add(itemOne);
        amounts.add(itemTwo);

        Basket basket = new Basket();
        basket.add(itemOne);
        basket.add(itemTwo);

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(amounts);
        basket.addDiscount(discountBuyOneGetOneFree);

        Assert.assertEquals(new BigDecimal("1.50"), basket.total());

        basket.removeDiscount(discountBuyOneGetOneFree);
        Assert.assertEquals(new BigDecimal("1.99"), basket.total());
    }


    @Test
    void basketWithOneDiscountForSecondItemFreeAndOneDiscountForWeight() {
        Item itemOne = new Product(new BigDecimal("0.49")).oneOf();
        Item itemTwo = new Product(new BigDecimal("1.50")).oneOf();

        Item itemThree = new WeighedProduct(new BigDecimal("100")).weighing(new BigDecimal(1));

        List<Item> amounts = new ArrayList<>();
        amounts.add(itemOne);
        amounts.add(itemTwo);
        amounts.add(itemThree);

        Basket basket = new Basket();
        basket.add(itemOne);
        basket.add(itemTwo);
        basket.add(itemThree);

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(basket.items());
        basket.addDiscount(discountBuyOneGetOneFree);

        Discount discountForWeight = new Discount1KiloHalfPrice(basket.items());
        basket.addDiscount(discountForWeight);

        Assert.assertEquals(new BigDecimal("51.50"), basket.total());

    }

}
