package kata.supermarket;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Test class for the promo related to the number of units bought
 */
class DiscountBuyOneGetOneFreeTest {

    @Test
    void applyDiscountWhenOnlyOneAmount() {
        Product product = new Product(new BigDecimal("6.00"));

        List<Item> originalAmounts = new ArrayList<>();
        originalAmounts.add(product.oneOf());

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(originalAmounts);
        discountBuyOneGetOneFree.calculateDiscount();

        Assert.assertEquals(Optional.empty(), discountBuyOneGetOneFree.getOffAmounts());
    }

    @Test
    void applyDiscountWhenThereAreTwoAmounts() {
        Product product1 = new Product(new BigDecimal("6.00"));
        Product product2 = new Product(new BigDecimal("3.50"));
        List<Item> originalAmounts = new ArrayList<>();
        originalAmounts.add(product1.oneOf());
        originalAmounts.add(product2.oneOf());

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(originalAmounts);
        discountBuyOneGetOneFree.calculateDiscount();

        Assert.assertEquals(new BigDecimal("6.00"), discountBuyOneGetOneFree.getOffAmounts().get().get(0));
    }

    @Test
    void applyDiscountWhenThereAreThreeAmounts() {
        Product product1 = new Product(new BigDecimal("6.00"));
        Product product2 = new Product(new BigDecimal("3.50"));
        Product product3 = new Product(new BigDecimal("4.00"));

        List<Item> originalAmounts = new ArrayList<>();
        originalAmounts.add(product1.oneOf());
        originalAmounts.add(product2.oneOf());
        originalAmounts.add(product3.oneOf());

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(originalAmounts);
        discountBuyOneGetOneFree.calculateDiscount();

        Assert.assertEquals(new BigDecimal("6.00"), discountBuyOneGetOneFree.getOffAmounts().get().get(0));
    }

    @Test
    void applyDiscountWhenThereAreFourAmounts() {
        Product product1 = new Product(new BigDecimal("6.00"));
        Product product2 = new Product(new BigDecimal("3.50"));
        Product product3 = new Product(new BigDecimal("4.00"));
        Product product4 = new Product(new BigDecimal("7.00"));

        List<Item> originalAmounts = new ArrayList<>();
        originalAmounts.add(product1.oneOf());
        originalAmounts.add(product2.oneOf());
        originalAmounts.add(product3.oneOf());
        originalAmounts.add(product4.oneOf());

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(originalAmounts);
        discountBuyOneGetOneFree.calculateDiscount();

        Assert.assertEquals(new BigDecimal("6.00"), discountBuyOneGetOneFree.getOffAmounts().get().get(0));
        Assert.assertEquals(new BigDecimal("4.00"), discountBuyOneGetOneFree.getOffAmounts().get().get(1));
    }

    @Test
    void applyDiscountWhenThereAreSixAmounts() {
        Product product1 = new Product(new BigDecimal("6.00"));
        Product product2 = new Product(new BigDecimal("3.50"));
        Product product3 = new Product(new BigDecimal("4.00"));
        Product product4 = new Product(new BigDecimal("3.00"));
        Product product5 = new Product(new BigDecimal("5.50"));
        Product product6 = new Product(new BigDecimal("7.00"));

        List<Item> originalAmounts = new ArrayList<>();
        originalAmounts.add(product1.oneOf());
        originalAmounts.add(product2.oneOf());
        originalAmounts.add(product3.oneOf());
        originalAmounts.add(product4.oneOf());
        originalAmounts.add(product5.oneOf());
        originalAmounts.add(product6.oneOf());

        Discount discountBuyOneGetOneFree = new DiscountBuyOneGetOneFree(originalAmounts);
        discountBuyOneGetOneFree.calculateDiscount();

        Assert.assertEquals(new BigDecimal("6.00"), discountBuyOneGetOneFree.getOffAmounts().get().get(0));
        Assert.assertEquals(new BigDecimal("4.00"), discountBuyOneGetOneFree.getOffAmounts().get().get(1));
        Assert.assertEquals(new BigDecimal("5.50"), discountBuyOneGetOneFree.getOffAmounts().get().get(2));
    }

}