package kata.supermarket;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the discount for weight itself
 * different items and weights and test the discount values
 */
class Discount1KiloHalfPriceTest {

    @Test
    void discountWhenBuyingHalfKilo() {
        WeighedProduct weighedProduct = new WeighedProduct(new BigDecimal("100"));
        Item item = weighedProduct.weighing(new BigDecimal("0.5"));

        List<Item> itemsToCalculate = Stream.of(item).collect(Collectors.toList());

        Assert.assertTrue(itemsToCalculate.contains(item));

        Discount1KiloHalfPrice discount1KiloHalfPrice = new Discount1KiloHalfPrice(itemsToCalculate);
        Optional<List<BigDecimal>> amounts = discount1KiloHalfPrice.calculateDiscount();

        Assert.assertEquals(new BigDecimal("50.00"), amounts.get().get(0).setScale(2));
    }

    @Test
    void discountWhenBuying1Kilo() {
        WeighedProduct weighedProduct = new WeighedProduct(new BigDecimal("100"));
        Item item = weighedProduct.weighing(new BigDecimal(1));

        List<Item> itemsToCalculate = Stream.of(item).collect(Collectors.toList());

        Assert.assertTrue(itemsToCalculate.contains(item));

        Discount1KiloHalfPrice discount1KiloHalfPrice = new Discount1KiloHalfPrice(itemsToCalculate);
        Optional<List<BigDecimal>> amounts = discount1KiloHalfPrice.calculateDiscount();

        Assert.assertEquals(new BigDecimal("50.00"), amounts.get().get(0).setScale(2));
    }


    @Test
    void discountWhenBuying2Kilos() {
        WeighedProduct weighedProduct = new WeighedProduct(new BigDecimal("100"));
        Item item = weighedProduct.weighing(new BigDecimal(2));

        List<Item> itemsToCalculate = Stream.of(item).collect(Collectors.toList());

        Assert.assertTrue(itemsToCalculate.contains(item));

        Discount1KiloHalfPrice discount1KiloHalfPrice = new Discount1KiloHalfPrice(itemsToCalculate);
        Optional<List<BigDecimal>> amounts = discount1KiloHalfPrice.calculateDiscount();

        Assert.assertEquals(new BigDecimal("100.00"), amounts.get().get(0).setScale(2));
    }

    @Test
    void discountWhenBuyingTwoAndHalfKilos() {
        WeighedProduct weighedProduct = new WeighedProduct(new BigDecimal("100"));
        Item item = weighedProduct.weighing(new BigDecimal("2.5"));

        List<Item> itemsToCalculate = Stream.of(item).collect(Collectors.toList());

        Assert.assertTrue(itemsToCalculate.contains(item));

        Discount1KiloHalfPrice discount1KiloHalfPrice = new Discount1KiloHalfPrice(itemsToCalculate);
        Optional<List<BigDecimal>> amounts = discount1KiloHalfPrice.calculateDiscount();

        Assert.assertEquals(new BigDecimal("150.00"), amounts.get().get(0).setScale(2));
    }

    @Test
    void discountWhenBuyingOneKiloForItemAandOneKiloForItemB() {
        WeighedProduct weighedProduct = new WeighedProduct(new BigDecimal("100"));
        Item item = weighedProduct.weighing(new BigDecimal("1"));

        WeighedProduct weighedProduct2 = new WeighedProduct(new BigDecimal("50"));
        Item item2 = weighedProduct2.weighing(new BigDecimal("1"));

        List<Item> itemsToCalculate = Stream.of(item, item2).collect(Collectors.toList());

        Assert.assertTrue(itemsToCalculate.contains(item));
        Assert.assertTrue(itemsToCalculate.contains(item2));

        Discount1KiloHalfPrice discount1KiloHalfPrice = new Discount1KiloHalfPrice(itemsToCalculate);
        Optional<List<BigDecimal>> amounts = discount1KiloHalfPrice.calculateDiscount();

        Assert.assertEquals(new BigDecimal("50.00"), amounts.get().get(0).setScale(2));
        Assert.assertEquals(new BigDecimal("25.00"), amounts.get().get(1).setScale(2));
    }

    @Test
    void discountWhenBuyingOneHalfKiloForItemAandTwoHalfKilosForItemB() {
        WeighedProduct weighedProduct = new WeighedProduct(new BigDecimal("100"));
        Item item = weighedProduct.weighing(new BigDecimal("1.5"));

        WeighedProduct weighedProduct2 = new WeighedProduct(new BigDecimal("50"));
        Item item2 = weighedProduct2.weighing(new BigDecimal("2.5"));

        List<Item> itemsToCalculate = Stream.of(item, item2).collect(Collectors.toList());

        Assert.assertTrue(itemsToCalculate.contains(item));
        Assert.assertTrue(itemsToCalculate.contains(item2));

        Discount1KiloHalfPrice discount1KiloHalfPrice = new Discount1KiloHalfPrice(itemsToCalculate);
        Optional<List<BigDecimal>> amounts = discount1KiloHalfPrice.calculateDiscount();

        Assert.assertEquals(new BigDecimal("100.00"), amounts.get().get(0).setScale(2));
        Assert.assertEquals(new BigDecimal("75.00"), amounts.get().get(1).setScale(2));
    }
}