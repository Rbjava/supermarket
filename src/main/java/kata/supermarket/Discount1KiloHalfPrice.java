package kata.supermarket;

import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * An implementation class for a discount for weight related promo
 *
 */
public class Discount1KiloHalfPrice implements Discount{

    private List<Item> originalItems; //this contains the items to be determined for the promo
    private List<BigDecimal> offItems; //this would contain the value that is going to be taken off

    public Discount1KiloHalfPrice(List<Item> original) {
        this.originalItems = original;

    }

    @Override
    public Optional<List<BigDecimal>> calculateDiscount() {

        if(CollectionUtils.isEmpty(originalItems))
            return Optional.empty();

        //we reset this as we don't want to just keep adding the discount every time we calculate
        if(CollectionUtils.isEmpty(offItems) || offItems.size() > 0)
            offItems = new ArrayList<>();

        for(int a = 0; a< originalItems.size(); a++) {
            if(originalItems.get(a) instanceof ItemByWeight) {
                ItemByWeight weighedProduct = (ItemByWeight) originalItems.get(a);

                BigDecimal priceForOneKilo = weighedProduct.getProduct().weighing(BigDecimal.ONE).price();

                int howManyExactKilos = weighedProduct.getWeightInKilos().intValue();
                BigDecimal howManyDecimalKilo = weighedProduct.getWeightInKilos().subtract(new BigDecimal(howManyExactKilos));

                BigDecimal discountAmount = priceForOneKilo.multiply(new BigDecimal(howManyExactKilos)).multiply(new BigDecimal("0.5"));
                BigDecimal fullPriceForTheDecimalWeight = weighedProduct.getProduct().weighing(howManyDecimalKilo).price();

                offItems.add(discountAmount.add(fullPriceForTheDecimalWeight));

            }
        }

        return Optional.ofNullable(offItems);
    }

    @Override
    public Optional<List<BigDecimal>> getOffAmounts() {
        return Optional.ofNullable(offItems);
    }
}
