package kata.supermarket;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * This class is the place where coordination to the discount objects to start calculating the discounts
 * it then retains the total amount of the discount based on the different promos applied
 */
public class DiscountManager {

    private BigDecimal totalDiscount = BigDecimal.ZERO;

    public void applyDiscount(Discount discountToBeApplied) {

        Optional<List<BigDecimal>> discounts = discountToBeApplied.calculateDiscount();

        discounts.ifPresent(this::addUpDiscount);
    }

    public void removeDiscount(Discount discountToBeRemoved) {

        Optional<List<BigDecimal>> discounts = discountToBeRemoved.calculateDiscount();

        discounts.ifPresent(this::removeDiscount);
    }

    public BigDecimal addUpDiscount(List<BigDecimal> toBeAdded){
        totalDiscount = totalDiscount.add(toBeAdded.stream().reduce(BigDecimal.ZERO, BigDecimal::add));

        return totalDiscount;
    }

    public BigDecimal removeDiscount(List<BigDecimal> toBeRemoved){
        totalDiscount = totalDiscount.subtract(toBeRemoved.stream().reduce(BigDecimal.ZERO, BigDecimal::add));

        return totalDiscount.setScale(2);
    }


    public BigDecimal getTotalDiscount() {
        return this.totalDiscount.setScale(2);
    }
}
