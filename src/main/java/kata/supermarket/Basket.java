package kata.supermarket;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Basket {
    private final List<Item> items;
    private final List<Discount> discountsForBasket; //this one containing a list of discounts that could be applied

    public Basket() {
        this.items = new ArrayList<>();
        this.discountsForBasket = new ArrayList<>();
    }

    public void add(final Item item) {
        this.items.add(item);
    }

    //added this method to apply a discount to basket
    public void addDiscount(Discount discount) {
        this.discountsForBasket.add(discount);
    }

    public void removeDiscount(Discount discount) {
        this.discountsForBasket.remove(discount);
    }


    List<Item> items() {
        return Collections.unmodifiableList(items);
    }

    public BigDecimal total() {
        return new TotalCalculator().calculate();
    }

    private class TotalCalculator {
        private final List<Item> items;

        TotalCalculator() {
            this.items = items();
        }

        private BigDecimal subtotal() {
            return items.stream().map(Item::price)
                    .reduce(BigDecimal::add)
                    .orElse(BigDecimal.ZERO)
                    .setScale(2, RoundingMode.HALF_UP);
        }

        /**
         * TODO: This could be a good place to apply the results of
         *  the discount calculations.
         *  It is not likely to be the best place to do those calculations.
         *  Think about how Basket could interact with something
         *  which provides that functionality.
         */
        private BigDecimal discounts() {
            DiscountManager discountManager = new DiscountManager();

            for(Discount currentDiscount : discountsForBasket) {
                discountManager.applyDiscount(currentDiscount);
            }
            return discountManager.getTotalDiscount();
        }

        private BigDecimal calculate() {
            return subtotal().subtract(discounts());
        }
    }
}
