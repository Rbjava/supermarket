package kata.supermarket;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Interface for a discount
 * we would have different implementations of discount classes which would have their own way of calculating
 */
public interface Discount {

    Optional<List<BigDecimal>> calculateDiscount();

    Optional<List<BigDecimal>> getOffAmounts();
}
