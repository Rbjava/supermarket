package kata.supermarket;

import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * for simplicity, not going to compare higher price or lower price, etc
 * i am just going through a list of items which are applicable per unit
 * and for each first one of the pair, i give it for free
 */
public class DiscountBuyOneGetOneFree implements Discount{

    private List<Item> originalItems;
    private List<BigDecimal> offAmounts;

    public DiscountBuyOneGetOneFree(List<Item> original) {
        this.originalItems = original;

    }


    public Optional<List<BigDecimal>> calculateDiscount() {

        if(CollectionUtils.isEmpty(originalItems) || originalItems.size() == 1)
            return Optional.empty();

        //we reset this as we don't want to just keep adding the discount every time we calculate
        if(CollectionUtils.isEmpty(offAmounts) || offAmounts.size() > 0)
            offAmounts = new ArrayList<>();

        for(int a = 0; a< originalItems.size(); a=a+2) {
            if(originalItems.get(a) instanceof  ItemByUnit) {
                offAmounts.add(originalItems.get(a).price());
            }
        }
        return Optional.ofNullable(offAmounts);
    }

    public Optional<List<BigDecimal>> getOffAmounts() {
        return Optional.ofNullable(offAmounts);
    }
}
